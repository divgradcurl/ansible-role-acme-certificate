ACME cert
=========

This role creates/renews a TLS certificate using the [acme_certficate module}(ansible_acme).
Currently, this role uses the `http-01` ACME challenge type.
It implements the ACME challenge using [lighttpd](lighttpd) on port 80 in a [podman](podman) container.
If you have a service running on 80/tcp, this won't work unless you stop that service.
This role has only been tested on CentOS 8.

If you already have a Let's Encrypt account key, you can place the account key locally in `local_files_dir/account.key`.
The role will copy the key to the remote server.
Otherwise, the role will create a new account key and copy it into whatever `local_files_dir` is set to.

The CSR includes the `www` and `mail` subdomains of `domain_name`.

This role has been adapted from DigitalOcean's [How to Acquire a Let's Encrypt Certificate Using Ansible on Ubuntu 18.04][digital_ocean].

The main changes are:
1. The OS is CentOS 8 instead of Ubuntu 18.04.
2. The ACME challenge is implemented using lighttpd in a podman container.

Requirements
------------

* [acme_certificate module](ansible_acme)
* A domain name with DNS records that point to your host server.
* TCP port 80 is not being used by another service.
* OpenSSL 1.1.1 or greater.
* Root privileges.

Role Variables
--------------

`acme_email` and `domain_name` are the only required variables.

```
acme_email
```
Email used for notification for renewals.

```
domain_name
```
Your domain name.

These vairables are defined in the role's `defaults/main.yml` file:

```
acme_challenge_type: http-01
acme_version: 2
local_files_dir: files/acme-certificate


acme_directory: https://acme-v02.api.letsencrypt.org/directory
letsencrypt_dir: /etc/letsencrypt
letsencrypt_keys_dir: /etc/letsencrypt/keys
letsencrypt_csrs_dir: /etc/letsencrypt/csrs
letsencrypt_certs_dir: /etc/letsencrypt/certs
letsencrypt_account_dir: /etc/letsencrypt/account
letsencrypt_account_key: /etc/letsencrypt/account/account.key
```

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      become: yes
      vars:
        acme_email: email_address
        domain_name: domain_name
      roles:
         - acme-cert

TO-DO
-------

* Create a service that renews the cert before expiration.
* Allow variable SANs and loop through SAN subdomain requests.

See Also
---------

* [Let's Encrypt][letsencrypt]
* [Automated Certificate Management Environment (ACME)][acme]
* [Ansible ACME certificate][ansible_acme]
* [Certbot][certbot]


License
-------

Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)

Author Information
------------------

Nov 2020 - G. F. AbuAkel


[digital_ocean]: https://www.digitalocean.com/community/tutorials/how-to-acquire-a-let-s-encrypt-certificate-using-ansible-on-ubuntu-18-04
[letsencrypt]: https://letsencrypt.org/
[acme]: https://en.wikipedia.org/wiki/Automated_Certificate_Management_Environment
[ansible_acme]: https://docs.ansible.com/ansible/latest/modules/acme_certificate_module.html#acme-certificate-module
[certbot]: https://certbot.eff.org/
[podman]: https://podman.io
